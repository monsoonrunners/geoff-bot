%Robot Parameters
wheelR = 3.1e-2; %Wheel radius (m)
axleLength = 13.6e-2; %Distance between wheels (m)
ticksPerRot = 20; % Ticks per rotation for encoders
load wheelLUT %Look up table for platform motors
Ts = 0.01; %Sample time
load wheelMotorModel %Motor model for platform motors
SensorArray = [64.3e-3 35.4e-3; 67.5e-3 5e-3;67.5e-3 -5e-3;64.3e-3 -35.4e-3];